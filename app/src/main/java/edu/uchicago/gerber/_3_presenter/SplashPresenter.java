package edu.uchicago.gerber._3_presenter;

import android.os.AsyncTask;

import edu.uchicago.gerber._4_view.interfaces.SplashView;

/**
 * Created by billobob on 9/21/16.
 */

public class SplashPresenter extends Presenter<SplashView> {


    private int SIMUATE_LOADING_TIME = 1500;

    @Override
    public void onStart() {



        splashLoad();
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onStop() {
    }

    @Override
    public void onDestroy() {

    }

    public void splashLoad() {
        // Background code to load data on splash screen
        new AsyncTask<Void, Void, Void>() {


            //TODO reload places

            @Override
            public Void doInBackground(Void... params) {

                try {
                    Thread.sleep(SIMUATE_LOADING_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // Swap out for the place fragment
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                view.getRouter().showPlacesFragment();

            }
        }.execute(null, null, null);
    }
}
