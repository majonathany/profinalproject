package edu.uchicago.gerber._3_presenter;

import java.util.List;

import edu.uchicago.gerber._1_data.model.PlaceModel;
import edu.uchicago.gerber._2_interactor.DeletePlaceUseCase;
import edu.uchicago.gerber._4_view.interfaces.PlaceView;

/**
 * Created by jonathan on 6/4/17.
 */

public class DeleteOnlyPresenter extends Presenter<PlaceView> implements DeletePlaceUseCase.DeletePlaceCallback
{

    @Override
    public void onStart()
    {

    }

    @Override
    public void onResume()
    {

    }

    @Override
    public void onStop()
    {
    }

    @Override
    public void onDestroy()
    {

    }

    public void deletePlace(PlaceModel place)
    {
        DeletePlaceUseCase deletePlaceUseCase = new DeletePlaceUseCase(this);
        deletePlaceUseCase.execute(place);

    }

    @Override
    public void passPostExecute(List<PlaceModel> placeModels)
    {

    }
}
