package edu.uchicago.gerber._2_interactor;

import edu.uchicago.gerber.App;
import edu.uchicago.gerber._1_data.core.YelpSearchException;
import edu.uchicago.gerber._1_data.core.RestaurantDirectoryInterface;
import edu.uchicago.gerber._1_data.core.yelpapi.YelpResultsData;

/**
 * The FetchYelpResultsUseCase interacts with a {@link RestaurantDirectoryInterface}
 * to pull requested data from the Yelp API. As with all use cases should only be called in the
 * presenter. Takes a string (location) as an argument and returns an Object of type
 * YelpResultsData.
 */

public class FetchYelpResultsUseCase extends UseCase<String, Void, YelpResultsData> {


    /**
     * The interface Yelp use case callback.
     */
    public interface YelpUseCaseCallback extends UseCaseCallback<YelpResultsData>{}

    /**
     * Instantiates a new Fetch yelp results use case.
     *
     * @param caseCallback the callback implemented by the class which created this usecase.
     */
    public FetchYelpResultsUseCase(YelpUseCaseCallback caseCallback) {
        super(caseCallback);
    }

    @Override
    protected YelpResultsData doInBackground(String... params) {
        YelpResultsData yelpResultsData = null;
        try {
            yelpResultsData = App.getRestaurantDirectory().fetchData(params[0], params[1]);
        } catch (YelpSearchException e) {
            return null;
        }
        return yelpResultsData;
    }

    @Override
    protected void onPostExecute(YelpResultsData yelpResultsData) {

        callback.passPostExecute(yelpResultsData);
    }
}
