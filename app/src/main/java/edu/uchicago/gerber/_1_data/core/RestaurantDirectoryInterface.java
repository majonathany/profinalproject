package edu.uchicago.gerber._1_data.core;

import edu.uchicago.gerber._1_data.core.yelpapi.YelpResultsData;


/**
 * Interface representing abstraction of getting Restaurant data from yelp. Only this method
 * should be called from the UseCases requesting Yelp data.
 */

public interface RestaurantDirectoryInterface {

    /**
     * Fetch yelp results data as a {@link YelpResultsData} object.
     *
     * @param name     Place name
     * @param location Restaurant location (city or ZIP)
     * @return {@link YelpResultsData} object
     * @throws YelpSearchException Throw a {@link YelpSearchException} if an error occurs in retrieval
     */
    YelpResultsData fetchData(String name, String location) throws YelpSearchException;
}
