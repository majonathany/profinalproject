package edu.uchicago.gerber._1_data.core.yelpapi;

import java.io.Serializable;
import java.util.List;


/**
 * The full FourSquareLocation info for a {@link Business}. The FourSquareCoordinate receives its own POJO
 * should we ever want to use it for mapping purposes, but in the app only the address and city
 * are used.
 */
public class Location implements Serializable {

    /**
     * The FourSquareCoordinate.
     */
    public Coordinate coordinate;
    /**
     * The Address.
     */
    public List<String> address;
    /**
     * The City.
     */
    public String city;
    /**
     * The Postal code.
     */
    public String postal_code;


}
