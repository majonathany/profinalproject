package edu.uchicago.gerber._1_data.core.yelpapi;

import java.io.Serializable;
import java.util.List;

import edu.uchicago.gerber._1_data.model.PlaceModel;


/**
 * This is a simple POJO representation of the raw data returned from Yelp before it is
 * convereted to a {@link PlaceModel}. Note that
 * it contains a {@link Location} object which itself contains a {@link Coordinate} object
 * due to the nature of the data returned by yelp.
 */
public class Business implements Serializable
{

    public Business(String name, String url, String image_url, String phone, Location location, List<List<String>> categories, String rating_img_url)
    {
        this.name = name;
        this.url = url;
        this.image_url = image_url;
        this.phone = phone;
        this.location = location;
        this.categories = categories;
        this.rating_img_url = rating_img_url;
    }


    public String name = "";
    /**
     * The Url for the yelp site.
     */
    public String url = "";
    /**
     * URL for yelp thumbnail.
     */
    public String image_url = "";
    /**
     * Phone number.
     */
    public String phone = "";
    /**
     * A {@link Location} object, not yet a string.
     */
    public Location location;
    /**
     * The Categories; no POJO is created for this as we only pull one type of category. This will be a list of
     * categories like "restaurants" and so forth.
     */
    public List<List<String>> categories;
    /**
     * The Rating as a text string.
     */
    public String rating_img_url = "";

}
