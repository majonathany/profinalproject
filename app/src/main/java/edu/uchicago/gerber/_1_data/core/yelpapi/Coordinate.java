package edu.uchicago.gerber._1_data.core.yelpapi;

import java.io.Serializable;


/**
 * Coordinates for the {@link Business}
 */
public class Coordinate implements Serializable {

    /**
     * The Latitude.
     */
    public String latitude;
    /**
     * The Longitude.
     */
    public String longitude;


    /**
     * Instantiates a new FourSquareCoordinate.
     *
     * @param latitude  the latitude
     * @param longitude the longitude
     */
    public Coordinate(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }



}
