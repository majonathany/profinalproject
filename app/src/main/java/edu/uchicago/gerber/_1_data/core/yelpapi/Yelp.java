package edu.uchicago.gerber._1_data.core.yelpapi;

import com.google.gson.Gson;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;


/**
 * The Yelp class is a helper class for connecting to the Yelp API. Contains API keys,
 * authentication logic, method for pulling the {@link YelpResultsData}
 */
public class Yelp {

    private static final String CONSUMER_KEY = "jLCZg8Nrat0vAU7LAjEaWg";
    private static final String CONSUMER_SECRET = "Y6k88CQGBpCHZ8Lm766aTPGAn4w";
    private static final String TOKEN = "UvFYVADCw_G15dY7-91YtUlPbmBSxck-";
    private static final String TOKEN_SECRET = "F52dMzOdurjeHAwTSh8EZ48iGmk";

    OAuthService service;

    Token accessToken;


    public Yelp(){
        this.service = new ServiceBuilder().provider(YelpApi2.class).apiKey(CONSUMER_KEY).apiSecret(CONSUMER_SECRET).build();
        this.accessToken = new Token(TOKEN, TOKEN_SECRET);
    }


    /**
     * Returns up to 20 results for a given yelp search, returning all the items as one
     * {@link YelpResultsData} object. Connects to the yelp API with keys provided in this class.
     *
     * @param searchTerm the search term
     * @param city       the city, ZIP, etc.
     * @return the yelpresultsdata
     */
    public YelpResultsData searchMultiple(String searchTerm, String city) {

        // Execute a signed call to the Yelp service.
        OAuthService service = new ServiceBuilder().provider(YelpApi2.class).apiKey(CONSUMER_KEY).apiSecret(CONSUMER_SECRET).build();
        Token accessToken = new Token(TOKEN, TOKEN_SECRET);
        OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.yelp.com/v2/search");

        request.addQuerystringParameter("location", city);
        request.addQuerystringParameter("term", searchTerm);
        request.addQuerystringParameter("limit", "20");

        service.signRequest(accessToken, request);
        Response response = request.send();
        String rawData = response.getBody();

        YelpResultsData mYelpSearchResult = null;

        try {
            mYelpSearchResult = new Gson().fromJson(rawData, YelpResultsData.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mYelpSearchResult;
    }




}