package edu.uchicago.gerber._1_data.core;

import java.util.ArrayList;
import java.util.List;

import edu.uchicago.gerber.App;
import edu.uchicago.gerber._1_data.core.foursquareAPI.FourSquare;
import edu.uchicago.gerber._1_data.core.yelpapi.Business;
import edu.uchicago.gerber._1_data.core.yelpapi.Yelp;
import edu.uchicago.gerber._1_data.core.yelpapi.YelpResultsData;

/**
 * Implementation of the {@link RestaurantDirectoryInterface}. See that page for description of
 * methods.
 */

public class YelpWrapper implements RestaurantDirectoryInterface {


    @Override
    public YelpResultsData fetchData(String name, String location) throws YelpSearchException
    {
            Yelp yelpApi = new Yelp();
            YelpResultsData yelpSearchResultLocal = null;
            yelpSearchResultLocal = combineYelpAndFS(yelpApi.searchMultiple(name, location), new FourSquare().searchFourSquare(name, location));
            if(yelpSearchResultLocal.businesses == null) throw new YelpSearchException("No results" +
                    "found for search");
            App.setLastSearched(location);
            return yelpSearchResultLocal;
    }

    public YelpResultsData combineYelpAndFS(YelpResultsData yelp, YelpResultsData fs)
    {
        List<Business> completeList = new ArrayList<>();

        if (yelp.businesses.isEmpty() && fs.businesses.isEmpty())
            return null;
        else if (yelp.businesses.isEmpty() && !fs.businesses.isEmpty())
            return fs;
        else if (!yelp.businesses.isEmpty() && fs.businesses.isEmpty())
            return yelp;
        else
        {
            int i,j;
            j = 0;
            for (i = 1; i < yelp.businesses.size() && j < fs.businesses.size(); i++)
            {
                yelp.businesses.add(i, fs.businesses.get(j++));
            }
            while (j < fs.businesses.size())
            {
                yelp.businesses.add(fs.businesses.get(j++));
            }
            return yelp;
        }
    }
}
