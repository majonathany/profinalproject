package edu.uchicago.gerber._1_data.core.foursquareAPI;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import edu.uchicago.gerber._1_data.core.foursquareAPI.FourSquarePOJO.FourSquareImagePOJO.FourSquareImageQuery;
import edu.uchicago.gerber._1_data.core.foursquareAPI.FourSquarePOJO.FourSquareQuery;
import edu.uchicago.gerber._1_data.core.foursquareAPI.FourSquarePOJO.Venue;
import edu.uchicago.gerber._1_data.core.yelpapi.Business;
import edu.uchicago.gerber._1_data.core.yelpapi.Coordinate;
import edu.uchicago.gerber._1_data.core.yelpapi.Location;
import edu.uchicago.gerber._1_data.core.yelpapi.YelpResultsData;

/**
 * Created by jonathan on 6/1/17.
 */

public class FourSquare
{
    public static final String CLIENT_ID = "WSBT5VA4UZNFG1GWI0UPGWGOTEQF2TDECEZRIZQYO10G2VJD";
    public static final String CLIENT_SECRET = "F4TKJ4ZYMHVFBB55N4SJHMCPHODQN1VKMCDFSGZPUSD0LNY2";
    public static final String VERSION = "20170602";

    private Gson gson;

    public String apiURL;
    public String apiImageURL;

    public FourSquare()
    {
        gson = new Gson();
        apiURL = "https://api.foursquare.com/v2/venues/search?";
        apiImageURL = "https://api.foursquare.com/v2/venues/";
    }

    public YelpResultsData searchFourSquare(String searchTerm, String searchCity)
    {
        String finalURL = apiURL + "client_id=" + CLIENT_ID + "&client_secret="+CLIENT_SECRET;

        finalURL += "&near=" + searchCity + "&query=" + searchTerm + "&v=" + VERSION;

        YelpResultsData wrapper = new YelpResultsData();
        wrapper.businesses = new ArrayList<>();

        /*
        for (Venue venue: fourSquareQuery.getResponse().getVenues())
        {
            JSONObject jsonObject2 = null;

            FourSquareImageQuery fourSquareImageQuery = null;

            String imageURL;

            List<List<String>> categories = new ArrayList<>();
            List<String> rest = new ArrayList<>();
            rest.add("Restaurant");
            categories.add(rest);

            Location location = new Location();
            location.coordinate = new Coordinate(venue.getLocation().getLat().toString(), venue.getLocation().getLng().toString());
            List<String> addresses = new ArrayList<>();
            addresses.add(venue.getLocation().getAddress());
            location.address = addresses;
            location.city = venue.getLocation().getCity();
            location.postal_code = venue.getLocation().getPostalCode();

            imageURL = fourSquareImageQuery.getResponse().getVenue().getBestPhoto().getPrefix() + "original" + fourSquareImageQuery.getResponse().getVenue().getBestPhoto().getSuffix();
            HttpResponse<String> jsonResponseImage = Unirest.get(imageURL).asString();
            fourSquareImageQuery = gson.fromJson(, FourSquareImageQuery.class);

            wrapper.businesses.add(new Business(venue.getName(), venue.getUrl(), imageURL, venue.getContact().getPhone(), location, categories, "FourSquare"));
        }

        return wrapper;

        */
        HttpsURLConnection connection = null;

        JSONObject jsonObject = null;

        try {
            connection = (HttpsURLConnection) new URL(finalURL).openConnection();
            int response = connection.getResponseCode();

            if (response == HttpURLConnection.HTTP_OK) {
                StringBuilder builder = new StringBuilder();
                BufferedReader reader = null;

                try
                {
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;

                    while ((line = reader.readLine()) != null)
                    {
                        builder.append(line);
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

                jsonObject = new JSONObject(builder.toString());

                FourSquareQuery fourSquareQuery = gson.fromJson(jsonObject.toString(), FourSquareQuery.class);

                for (Venue venue: fourSquareQuery.getResponse().getVenues())
                {
                    JSONObject jsonObject2 = null;

                    FourSquareImageQuery fourSquareImageQuery = null;

                    String imageURLJSON = "";

                    List<List<String>> categories = new ArrayList<>();
                    List<String> rest = new ArrayList<>();
                    rest.add("Restaurant");
                    rest.add("Restaurant");
                    categories.add(rest);

                    Location location = new Location();
                    location.coordinate = new Coordinate(venue.getLocation().getLat().toString(), venue.getLocation().getLng().toString());
                    List<String> addresses = new ArrayList<>();
                    addresses.add(venue.getLocation().getAddress());
                    location.address = addresses;
                    location.city = venue.getLocation().getCity();
                    location.postal_code = venue.getLocation().getPostalCode();

                    imageURLJSON += apiImageURL + venue.getId() + "/photos?" + "client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&v=" + VERSION;

                    HttpsURLConnection connection2 = null;
                    try {
                        connection2 = (HttpsURLConnection) new URL(imageURLJSON).openConnection();
                        int response2 = connection2.getResponseCode();

                        if (response == HttpURLConnection.HTTP_OK) {
                            StringBuilder builder2 = new StringBuilder();
                            BufferedReader reader2 = null;

                            try
                            {
                                reader2 = new BufferedReader(new InputStreamReader(connection2.getInputStream()));
                                String line2;

                                while ((line2 = reader2.readLine()) != null)
                                {
                                    builder2.append(line2);
                                }
                            }
                            catch (IOException e) {
                                e.printStackTrace();
                            }

                            jsonObject2 = new JSONObject(builder2.toString());
                            fourSquareImageQuery = gson.fromJson(jsonObject2.toString(), FourSquareImageQuery.class);

                            String imageURL = fourSquareImageQuery.getResponse().getPhotos().getItems().get(0).getPrefix() + "original" + fourSquareImageQuery.getResponse().getPhotos().getItems().get(0).getSuffix();

                            wrapper.businesses.add(new Business(venue.getName(), venue.getUrl(), imageURL, venue.getContact().getPhone(), location, categories, "FourSquare"));
                        }
                    }
                    catch (Exception e) {
                        wrapper.businesses.add (new Business(venue.getName(), venue.getUrl(), "", venue.getContact().getPhone(), location, categories, "FourSquare"));
                        e.printStackTrace();
                    }
                }
                return wrapper;

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
