package edu.uchicago.gerber._4_view.interfaces;

import edu.uchicago.gerber._1_data.core.yelpapi.YelpResultsData;
import edu.uchicago.gerber._4_view.frags.NewFragment;


/**
 * The View for the {@link NewFragment}.
 */
public interface NewView extends BaseView {

    /**
     * Display search results. Puts the YelpResultsData in a bundle and fires off to a
     * ResultsFragment.
     *
     * @param results the YelpResultsData from a search.
     */
    void displaySearchResults(YelpResultsData results);

    void refreshFields();
}
