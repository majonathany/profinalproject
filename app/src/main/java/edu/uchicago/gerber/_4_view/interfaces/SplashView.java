package edu.uchicago.gerber._4_view.interfaces;

import edu.uchicago.gerber._4_view.frags.SplashFragment;

/**
 * View for the {@link SplashFragment}.
 * This is a simple fragment so no methods are defined here.
 */

public interface SplashView extends BaseView {
}
