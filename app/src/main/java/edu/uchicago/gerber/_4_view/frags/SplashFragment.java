package edu.uchicago.gerber._4_view.frags;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import edu.uchicago.gerber._3_presenter.SplashPresenter;
import edu.uchicago.gerber._4_view.interfaces.SplashView;

/**
 * Created by agerber on 9/12/2016.
 */
public class SplashFragment extends BaseFragment<SplashPresenter> implements SplashView {


    public static SplashFragment newInstance() {
        return new SplashFragment();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(edu.uchicago.gerber.R.layout.frag_splash, container, false);
        getActivity().overridePendingTransition(
                edu.uchicago.gerber.R.anim.slide_in_right,
                edu.uchicago.gerber.R.anim.slide_out_left
        );
        ImageView splashImg = (ImageView) v.findViewById(edu.uchicago.gerber.R.id.iv_splash);
        Glide.with(getContext())
                .load(edu.uchicago.gerber.R.drawable.cityscape).centerCrop()
                .into(splashImg);
        // Load the sleeping async task and dummy data
        return v;
    }


    @Override
    protected SplashPresenter createPresenter() {
        return new SplashPresenter();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
