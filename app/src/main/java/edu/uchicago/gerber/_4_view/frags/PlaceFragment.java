package edu.uchicago.gerber._4_view.frags;

import android.graphics.ColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import edu.uchicago.gerber.App;
import edu.uchicago.gerber.R;
import edu.uchicago.gerber._1_data.model.PlaceModel;
import edu.uchicago.gerber._2_interactor.DeletePlaceUseCase;
import edu.uchicago.gerber._3_presenter.DeleteOnlyPresenter;
import edu.uchicago.gerber._3_presenter.PlacePresenter;
import edu.uchicago.gerber._3_presenter.adapters.PlaceAdapter;
import edu.uchicago.gerber._3_presenter.utils.PrefsMgr;
import edu.uchicago.gerber._4_view.interfaces.PlaceView;

import static com.facebook.FacebookSdk.getApplicationContext;


public class PlaceFragment extends BaseFragment<PlacePresenter> implements PlaceView
{
    protected TreeMap<Integer, PlaceModel> listOfPlacesToDelete;

    protected ListView listView;
    protected GridView gridView;

    protected ProgressBar progress_bar;
    protected TextView emptyTextView;

    protected PlaceAdapter mPlaceAdapter;
    private ImageView mImageView;

    public boolean displayDelete;


    public enum Type
    {
        LIST, GRID
    }


    public PlaceFragment()
    {

    }

    public static PlaceFragment newInstance(Type type)
    {

        PlaceFragment placeFragment = new PlaceFragment();
        Bundle bundle = new Bundle();
        bundle.putString("TYPE", type.toString());
        placeFragment.setArguments(bundle);
        return placeFragment;
    }

    @Override
    protected PlacePresenter createPresenter()
    {
        presenter = new PlacePresenter();
        return presenter;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        listOfPlacesToDelete = new TreeMap<>();
        View view;

        PrefsMgr.setString(getApplicationContext(), App.KEY_COUNTRY_CODE, "US");

        if (getArguments().getString("TYPE").equals(Type.LIST.toString()))
        {
            view = inflater.inflate(edu.uchicago.gerber.R.layout.fragment_place_list, container, false);
            listView = (ListView) view.findViewById(edu.uchicago.gerber.R.id.place_list_view);
        }
        else
        {
            view = inflater.inflate(edu.uchicago.gerber.R.layout.fragment_place_grid, container, false);
            gridView = (GridView) view.findViewById(edu.uchicago.gerber.R.id.place_grid_view);

        }


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(edu.uchicago.gerber.R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getRouter().showEditFragment(null);
            }
        });

        progress_bar = (ProgressBar) view.findViewById(edu.uchicago.gerber.R.id.progress_bar);
        progress_bar.setVisibility(View.INVISIBLE);
        emptyTextView = (TextView) view.findViewById(edu.uchicago.gerber.R.id.empty_textview);

        final DeleteOnlyPresenter deleteOnlyPresenter = new DeleteOnlyPresenter();

        final FloatingActionButton deleteButton = (FloatingActionButton) view.findViewById(R.id.dab);
        deleteButton.hide();
        deleteButton.setOnClickListener(new View.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(View v)
                                            {
                                                for (PlaceModel p: listOfPlacesToDelete.values())
                                                {
                                                    deleteOnlyPresenter.deletePlace(p);
                                                }
                                                deleteButton.hide();
                                                getRouter().recreate();


                                            }
                                        }
        );

        listView = (ListView) view.findViewById(edu.uchicago.gerber.R.id.place_list_view);

        displayDelete = false;

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id)
            {
                if (displayDelete == false)
                {
                    deleteButton.show();
                }

                if (((ColorDrawable) (((ViewGroup) ((ViewGroup) arg1).getChildAt(0)).getChildAt(0).getBackground())).getColor() == getResources().getColor(R.color.turquoise))
                {
                    listOfPlacesToDelete.remove(pos);
                    ((ViewGroup) ((ViewGroup) arg1).getChildAt(0)).getChildAt(0).setBackgroundColor(getResources().getColor(R.color.black));
                    ((ViewGroup) (arg1)).getChildAt(2).setBackgroundColor(getResources().getColor(R.color.black));
                }
                else
                {
                    listOfPlacesToDelete.put(pos, mPlaceAdapter.getItem(pos));
                    ((ViewGroup) ((ViewGroup) arg1).getChildAt(0)).getChildAt(0).setBackgroundColor(getResources().getColor(R.color.turquoise));
                    ((ViewGroup) (arg1)).getChildAt(2).setBackgroundColor(getResources().getColor(R.color.turquoise));
                }
                return true;
            }
        });


        return view;
    }


    @Override
    public void startProgress()
    {
        super.startProgress();
        progress_bar.setVisibility(View.VISIBLE);

    }

    @Override
    public void stopProgress()
    {
        super.stopProgress();
        progress_bar.setVisibility(View.INVISIBLE);
    }

    //this method is defined in the PlaceView interface and is called from the presenter (PlacesPresenter)
    @Override
    public void displayPlaces(PlaceAdapter placeAdapter, List<PlaceModel> places)
    {

        if (mPlaceAdapter == null)
        {
            mPlaceAdapter = placeAdapter;
            if (getArguments().getString("TYPE").equals(Type.GRID.toString()))
            {
                gridView.setAdapter(mPlaceAdapter);
            }
            else
            {
                listView.setAdapter(mPlaceAdapter);
            }

        }
        mPlaceAdapter.setResults(places);
        mPlaceAdapter.notifyDataSetChanged();

        if (places == null || places.size() == 0)
        {

            if (getArguments().getString("TYPE").equals(Type.GRID.toString()))
            {
                gridView.setEmptyView(emptyTextView);
            }
            else
            {
                listView.setEmptyView(emptyTextView);
            }


        }
    }

    @Override
    public void onStop()
    {
        super.onStop();
        mPlaceAdapter = null;
    }
}
