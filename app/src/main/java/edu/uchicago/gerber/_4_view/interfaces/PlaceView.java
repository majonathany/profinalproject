package edu.uchicago.gerber._4_view.interfaces;

import java.util.List;

import edu.uchicago.gerber._1_data.model.PlaceModel;
import edu.uchicago.gerber._3_presenter.adapters.PlaceAdapter;


/**
 * The View for the PlaceFragment.
 */
public interface PlaceView extends BaseView {

    /**
     * Display places.
     *
     * @param placeAdapter the place adapter
     * @param places       the places [list]
     */
    void displayPlaces(PlaceAdapter placeAdapter, List<PlaceModel> places);

}
