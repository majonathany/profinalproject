package edu.uchicago.gerber._4_view.frags;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.PopupMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.uchicago.gerber.R;
import edu.uchicago.gerber._1_data.core.yelpapi.YelpResultsData;
import edu.uchicago.gerber._3_presenter.NewPresenter;
import edu.uchicago.gerber._3_presenter.utils.PrefsMgr;
import edu.uchicago.gerber._4_view.interfaces.NewView;

import static android.app.Activity.RESULT_OK;


/**
 * The NewFragment is the fragment displayed when a user is searching for Restaurants. A fairly
 * basic fragment which has an EditText for the city/location to search and a search button.
 *
 * Passes on the {@link YelpResultsData} to the {@link ResultsFragment}.
 */

public class NewFragment extends BaseFragment<NewPresenter> implements NewView, View.OnClickListener
{
    public static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;

    private EditText mCityField, mNameField;
    private Button mExtractButton, mClearButton, mSpeechButton;

    private TextView mLocationField;

    public static boolean nearbyOnly;

    //this is a proxy to our database
    private InputMethodManager mImm;
    private ProgressDialog progressDialog;

    private boolean mArgsPut = false;


    /**
     * The Enter listener for the City EditText.
     */
    EditText.OnKeyListener enterListener = new EditText.OnKeyListener()
    {

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event)
        {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER))
            {
                // Perform action on key press

                mImm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return true;
            }
            return false;
        }
    };


    /**
     * Instantiate a NewFragment. The text was previously used for sharing into the app
     * (e.g. through a yelp link) which is not in the final demo app, but if re-implemented
     * a shared link should be directed to the NewFragment.
     *
     * @param text the text shared (not used in demo app)
     * @return the new fragment
     */
    public static NewFragment newInstance(String text)
    {
        NewFragment fragment = new NewFragment();
        Bundle args = new Bundle();
        if (text != null)
        {
            args.putSerializable("SHARE", text);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = inflater.inflate(edu.uchicago.gerber.R.layout.frag_scroll_new, container, false);


        mCityField = (EditText) v.findViewById(edu.uchicago.gerber.R.id.edit_main_city);
        mNameField = (EditText) v.findViewById(edu.uchicago.gerber.R.id.edit_main_name);

        mExtractButton = (Button) v.findViewById(edu.uchicago.gerber.R.id.extract_yelp_button);
        mClearButton = (Button) v.findViewById(edu.uchicago.gerber.R.id.clear_button);
        mSpeechButton = (Button) v.findViewById(R.id.speech_button);

        mLocationField = (TextView) v.findViewById(R.id.locationField);

        mImm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);

        mCityField.setOnKeyListener(enterListener);
        mCityField.setText(PrefsMgr.getString(getContext(), PrefsMgr.CITY));

        mExtractButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                presenter.performSearch(mNameField.getText().toString(), mCityField.getText().toString());

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mCityField.getWindowToken(), 0);
            }
        });

        mClearButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mCityField.setText("");
                mNameField.setText("");
                presenter.clearFields();
            }

        });



        PackageManager pm = getRouter().getPackageManager();
        List activities = pm.queryIntentActivities(new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        if (activities.size() != 0) {
            mSpeechButton.setOnClickListener(this);
        } else {
            mSpeechButton.setEnabled(false);
            mSpeechButton.setText("Speech N/A");
        }

        nearbyOnly = PrefsMgr.getBoolean(getRouter(), "search_nearby", false);

        if (!nearbyOnly)
        {
            mCityField.setVisibility(View.VISIBLE);
        }

        if (nearbyOnly)
        {
            mCityField.setVisibility(View.INVISIBLE);

            LocationManager lm = (LocationManager) getRouter().getSystemService(Context.LOCATION_SERVICE);
            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();

            try
            {
                if (getRouter().getGeocoder().getFromLocation(latitude, longitude, 1).size() != 0)
                {
                    String locality = getRouter().getGeocoder().getFromLocation(latitude, longitude, 1).get(0).getLocality();
                    mLocationField.setText("Current Location: " + locality);
                    mCityField.setText((CharSequence) locality);
                }
            }
            catch (IOException e)
            {
                Toast.makeText(getRouter(), "Cannot find location, please input city", Toast.LENGTH_LONG);
                mCityField.setVisibility(View.VISIBLE);
            }
        }

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    protected NewPresenter createPresenter()
    {
        return new NewPresenter();
    }

    /**
     * Progress dialog displayed when searching for results.
     */
    @Override
    public void startProgress()
    {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Fetching data");
        progressDialog.setMessage("One moment please...");
        progressDialog.setCancelable(true);

        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                presenter.cancelSearch();
                progressDialog.dismiss();
            }
        });
        progressDialog.show();
    }

    @Override
    public void stopProgress()
    {
        if (progressDialog != null)
        {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    @Override
    public void showToast(String text)
    {
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Call back to the single Activity that hosts all frags. When .showResultFragment() is shown,
     * then onStart(), onResume() and eventually onStop() will be called
     *
     * @param results the YelpResultsData from a search.
     */
    @Override
    public void displaySearchResults(YelpResultsData results)
    {

        getRouter().showResultFragment(results);
    }


    @Override
    public void refreshFields()
    {

        FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.detach(this);
        fragTransaction.attach(this);
        fragTransaction.commit();

    }

    @Override
    public void onClick(View v)
    {
        startVoiceRecognitionActivity();
    }

    /*
    public void voiceinputbuttons()
    {
        mSpeechButton = (Button) getRouter().findViewById(R.id.speech_button);
        mListView = (ListView) getRouter().findViewById(R.id.listView);
    }*/

    public void startVoiceRecognitionActivity()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Say a Place:");
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 5000000);
        startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK)
        {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches.size() >= 1)
            {
                fillEditText(matches.get(0));
            }
            else
            {

            }
        }
    }
    public void fillEditText(String phrase)
    {
        if (phrase.contains(" in "))
        {
            int index = phrase.indexOf(" in ");
            String restaurant = phrase.substring(0, index).trim();
            String city;
            if (index + 3 < phrase.length())
            {
                city = phrase.substring(index + 3, phrase.length()).trim();
            }
            else
            {
                city = "";
            }
            mNameField.setText(restaurant);
            mCityField.setText(city);

            if (PrefsMgr.getBoolean(getRouter(), "search_nearby", false))
            {
                if (!city.equals(mCityField.getText().toString()))
                    mLocationField.setText("Changed Location: " + city);
                else
                    mLocationField.setText("Location: " + city);
            }
        }
        else
            mNameField.setText(phrase);
    }
}

