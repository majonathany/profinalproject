Readme:

I've completed the required features as well as the Twitter feature. I've implemented the following features:

+ Added long-clicking to enable deletion
+ Added speech button
+ In non-nearby mode, it edits the text in the EditText
+ In nearby mode, it edits text in TextView
+ Added a Settings Activity or Fragment (you can use the wizard)
+ Nearby mode can be set on using switch
+ Changed color of dialog to match rest of app
+ Formatted the phone number in the Edit Form so that it appears like so (773) 456-7890
+ Added FourSquare and wrapped it around Yelp
+ Integrated Facebook using ShareDialog